﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifepointController : MonoBehaviour
{
    [SerializeField]
    private List<GameObject> lifePoints = new List<GameObject>();
    [SerializeField]
    private CubeController target = null;

    void Start()
    {
        target.changedLifepoints += ChangedLifepoints;
    }

    void ChangedLifepoints(int lifepointsLeft) {
        int i = 0;
        foreach(GameObject lifePoint in lifePoints) {
            if(i < lifepointsLeft)
                lifePoint.SetActive(true);
            else
                lifePoint.SetActive(false);
            
            i++;
        }
    }
}
