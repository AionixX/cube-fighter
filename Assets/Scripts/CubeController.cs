﻿using System.Collections;
using System.Collections.Generic;
using Unity.MLAgents.Actuators;
using Unity.MLAgents.Sensors;
using UnityEngine;

public class CubeController : CubeFighter
{
    

    public override void CollectObservations(VectorSensor sensor)
    {
        sensor.AddObservation(transform.localPosition);
        sensor.AddObservation(transform.rotation);
        sensor.AddObservation((int)state);
        sensor.AddObservation(IsAlife());

        foreach (CubeController cube in gameController.players)
        {
            if (cube == this)
                continue;

            sensor.AddObservation(cube.transform.localPosition);
            sensor.AddObservation(cube.transform.rotation);
            sensor.AddObservation((int)cube.state);
            sensor.AddObservation(cube.IsAlife());
        }
    }

    public override void OnEpisodeBegin()
    {
        ResetCube();
    }
    
    public override void OnActionReceived(float[] vectorAction)
    {
        AddReward(1 / MaxStep);
        ActionInput newAction = new ActionInput();
        newAction.movement.y = vectorAction[0];
        newAction.movement.x = vectorAction[1];
        newAction.jump = vectorAction[2] == 1 ? true : false;
        newAction.dash = vectorAction[3] == 1 ? true : false;
        action = newAction;
    }

    public override void Heuristic(float[] actionsOut)
    {
        if (!isPlayer) return;

        actionsOut[0] = InputController.Instance.GetVertical();
        actionsOut[1] = InputController.Instance.GetHorizontal();
        actionsOut[2] = InputController.Instance.GetJump() ? 1 : 0;
        actionsOut[3] = InputController.Instance.GetDash() ? 1 : 0;
    }

    public override void KilledEnemy()
    {
        base.KilledEnemy();
        AddReward(1.0f);
    }

    public override void Lose()
    {
        base.Lose();
        AddReward(-1.0f);
    }

    public override void GetHit(GameObject _enemy)
    {
        if (state == FighterStates.Confused) return;
        base.GetHit(_enemy);

        AddReward(-0.1f);
    }

    public override void HittedEnemy()
    {
        base.HittedEnemy();
        AddReward(0.1f);
    }
}
