﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using Unity.MLAgents;
using System;

public enum FighterStates
{
    Moving,
    Jumping,
    RotateToJumpAttack,
    JumpAttack,
    Dashing,
    Confused,
    Dead
}

public enum Axis
{
    x,
    y,
    z
}

public enum InterpolationMode
{
    Linear,
    Spherical
}

public class ActionInput
{
    public Vector2 movement = Vector2.zero;
    public bool jump = false;
    public bool dash = false;
}

[RequireComponent(typeof(Rigidbody))]
public class CubeFighter : Agent
{
    public Action<int> changedLifepoints;
    public Action Moving;
    public Action Dashing;
    public Action Jumping;
    public Action JumpAttacking;
    public Action JumpAttackHitGround;
    public Action Confused;
    public Action Dead;


    protected Rigidbody rb;
    public FighterStates state;
    protected ActionInput action;
    protected bool isOnGround = false;
    protected Vector3 holdPosition = Vector3.zero;
    protected bool canDash = true;
    protected bool canJumpAttack = true;
    protected bool canTakeAction = true;
    protected bool canJump = true;
    protected bool jumpIntersected = false;
    protected bool dashIntersected = false;
    protected float holdingTime = 0f;
    protected float rotatedOnHeadTime = 0f;
    protected int lifesLeft = 0;
    protected CubeController lastHit = null;


    [SerializeField]
    protected float maxRndX = 13f;
    [SerializeField]
    protected float maxRndZ = 13f;
    [SerializeField]
    public bool isPlayer = false;
    [SerializeField]
    protected GameController gameController = null;
    [SerializeField]
    protected int maxLifes = 3;
    [SerializeField]
    protected float speed = 10f;
    [SerializeField]
    protected float acceleration = 4f;
    [SerializeField]
    protected float movementDampingThreshold = 0.3f;
    [SerializeField]
    protected float movementDamping = 5f;
    [SerializeField]
    protected float rotationSpeed = 5f;
    [SerializeField]
    protected float dashSpeed = 20f;
    [SerializeField]
    protected float dashAcceleration = 20f;
    [SerializeField]
    protected float dashDistance = 5f;
    [SerializeField]
    protected float dashForce = 15f;
    [SerializeField]
    protected float dashCooldown = 1.5f;
    [SerializeField]
    protected float jumpAttackRadius = 3f;
    [SerializeField]
    protected float jumpHeight = 10f;
    [SerializeField]
    protected float jumpCooldown = 1f;
    [SerializeField]
    protected float velocityDampingOnJump = 0.3f;
    [SerializeField]
    protected float jumpRollSpeed = 2;
    [SerializeField]
    protected float jumpAttackGravity = 20f;
    [SerializeField]
    protected float jumpCoolDownAfterAttack = 0.3f;
    [SerializeField]
    protected float jumpAttackForce = 20f;
    [SerializeField]
    protected float jumpAttackCooldown = 1.5f;
    [SerializeField]
    protected float jumpGravity = 3f;
    [SerializeField]
    protected float maxHoldingTime = 0.5f;
    [SerializeField, Range(0, 2)]
    protected float applyCenterOfMassAt = 0.1f;
    [SerializeField]
    protected float actionTimeout = 0.1f;
    [SerializeField]
    protected float confusedAfterHitTime = 3f;
    [SerializeField]
    protected float respawnTime = 3f;
    [SerializeField]
    protected float respawnHeight = 5f;
    [SerializeField]
    protected float lasthitResetTime = 1.0f;


    void Start()
    {
        rb = GetComponent<Rigidbody>();
        ResetCube();
    }

    void FixedUpdate()
    {
        rb.centerOfMass = Vector3.down * applyCenterOfMassAt;

        DoAction();

        EvaluateState();
    }

    protected void ResetCube()
    {
        lifesLeft = maxLifes;
        changedLifepoints?.Invoke(lifesLeft);

        lastHit = null;
        canDash = true;
        canJumpAttack = true;
        canTakeAction = true;
        action = null;
        jumpIntersected = false;
        dashIntersected = false;
        holdingTime = 0f;
        rotatedOnHeadTime = 0f;

        ResetPosition();
        state = FighterStates.Jumping;
    }

    protected void ResetPosition()
    {
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;

        float rndX = UnityEngine.Random.Range(-maxRndX, maxRndX);
        float rndZ = UnityEngine.Random.Range(-maxRndZ, maxRndZ);
        transform.localPosition = new Vector3(rndX, respawnHeight, rndZ);

        Quaternion rndRot = Quaternion.Euler(0, UnityEngine.Random.Range(-180, 180), 0);
        transform.rotation = rndRot;
    }
    public virtual void GetHit(GameObject _enemy)
    {
        if (state == FighterStates.Confused) return;

        _enemy.SendMessage("HittedEnemy");

        StopAllCoroutines();

        StartCoroutine(HitTimeout());
    }

    public virtual void HittedEnemy() {
        // Do Stuff
    }

    public virtual void KilledEnemy()
    {
        // Do stuff
    }

    public bool IsAlife()
    {
        return lifesLeft > 0;
    }

    public virtual void Lose()
    {
        gameController.PlayerLost(this);
    }

    void DoAction()
    {
        if (action == null) return;

        if (action.jump)
        {
            if (CanJump())
            {
                StartCoroutine(Jump());
                StartCoroutine(ActionTimeout(actionTimeout));
            }
            else if (CanJumpAttack())
            {
                holdPosition = transform.position;
                holdingTime = 0f;
                state = FighterStates.RotateToJumpAttack;
                StartCoroutine(JumpAttackCooldown(jumpAttackCooldown));
            }
        }
        else if (action.dash)
        {
            if (CanDash())
            {
                Dashing?.Invoke();
                state = FighterStates.Dashing;
                StartCoroutine(Dash());
                StartCoroutine(DashCooldown(dashCooldown));
            }
        }
        else
        {
            if (CanMove())
            {
                Vector3 dir = new Vector3(action.movement.x, 0, action.movement.y);
                Move(dir);
                Rotate(dir, rotationSpeed);
            }
        }
    }

    bool CanMove()
    {
        return state == FighterStates.Moving && isOnGround && AxisInRightRotations();
    }

    bool CanJump()
    {
        return state == FighterStates.Moving && isOnGround && AxisInRightRotations() && canTakeAction && canJump;
    }

    bool CanJumpAttack()
    {
        return state == FighterStates.Jumping && canTakeAction && !jumpIntersected && canJumpAttack;
    }

    bool CanDash()
    {
        return state == FighterStates.Moving && canDash && AxisInRightRotations();
    }

    void EvaluateState()
    {
        switch (state)
        {
            case FighterStates.Moving:
                EvaluateMoving();
                break;
            case FighterStates.Jumping:
                EvaluateJumping();
                break;
            case FighterStates.RotateToJumpAttack:
                EvaluateRotateToJumpAttack();
                break;
            case FighterStates.JumpAttack:
                EvaluateJumpAttack();
                break;
            case FighterStates.Dashing:
                EvaluateDashing();
                break;
            case FighterStates.Confused:
                EvaluateConfused();
                break;
            default:
                break;
        }
    }

    void EvaluateMoving()
    {
        jumpIntersected = false;
        dashIntersected = false;

        if(action == null) return; 

        if (action.movement.magnitude < movementDampingThreshold && AxisInRightRotations() && isOnGround)
        {
            Vector3 newVel = Vector3.Lerp(rb.velocity, Vector3.zero, movementDamping * Time.deltaTime);
            rb.velocity = newVel;
        }
    }

    void EvaluateJumping()
    {
        Vector3 vel = rb.velocity;
        vel.y = Mathf.Lerp(rb.velocity.y, -jumpGravity, Time.deltaTime);
        rb.velocity = vel;

        if (isOnGround)
        {
            state = FighterStates.Moving;
        }
    }

    void EvaluateRotateToJumpAttack()
    {
        transform.position = holdPosition;
        holdingTime += Time.deltaTime;
        if (jumpIntersected || holdingTime > maxHoldingTime)
        {
            state = FighterStates.Jumping;
            return;
        }

        if (AxisInRightRotations())
        {
            state = FighterStates.JumpAttack;
            rb.velocity = new Vector3(0, -jumpAttackGravity, 0);
        }
    }

    void EvaluateJumpAttack()
    {
        if (isOnGround)
        {
            StartCoroutine(ActionTimeout(jumpCoolDownAfterAttack));
            state = FighterStates.Moving;
        }
    }

    void EvaluateDashing() { }

    void EvaluateConfused() { }

    bool AxisInRightRotations()
    {
        if (Mathf.Abs(transform.rotation.x) * Mathf.Rad2Deg > 3f)
        {
            return false;
        }
        if (Mathf.Abs(transform.rotation.z) * Mathf.Rad2Deg > 3f)
        {
            return false;
        }
        return true;
    }

    void Move(Vector3 _dir)
    {
        Vector3 movement = _dir * acceleration * Time.deltaTime;
        movement.y = 0;
        rb.AddForce(movement, ForceMode.VelocityChange);
        rb.velocity = Vector3.ClampMagnitude(rb.velocity, speed);
    }

    void Rotate(Vector3 _dir, float _speed)
    {
        Quaternion newRot = transform.rotation;
        if (_dir != Vector3.zero)
            newRot.SetLookRotation(_dir);

        transform.rotation = Quaternion.Slerp(transform.rotation, newRot, rotationSpeed * Time.deltaTime);
    }

    IEnumerator Dash()
    {
        float distance = 0f;
        while (distance < dashDistance && !dashIntersected)
        {
            Vector3 startPos = transform.position;
            Vector3 movement = transform.forward * dashAcceleration * Time.deltaTime;
            movement.y = 0;
            rb.AddForce(movement, ForceMode.VelocityChange);
            rb.velocity = Vector3.ClampMagnitude(rb.velocity, dashSpeed);
            distance += movement.magnitude;
            yield return null;
        }
        rb.velocity = Vector3.ClampMagnitude(rb.velocity, speed);
        state = FighterStates.Moving;
    }

    IEnumerator RotateTo(Quaternion _target, float _rotSpeed)
    {
        Quaternion from = transform.rotation;
        for (float t = 0f; t < 1f; t += _rotSpeed * Time.deltaTime)
        {
            transform.rotation = Quaternion.Slerp(from, _target, t);
            yield return null;
        }
        transform.rotation = _target;
    }

    IEnumerator RotateTo(Vector3 _target, float _rotSpeed)
    {
        Vector3 from = transform.rotation.eulerAngles;
        for (float t = 0f; t < 1f; t += _rotSpeed * Time.deltaTime)
        {
            transform.rotation = Quaternion.Euler(Vector3.Slerp(from, _target, t));
            yield return null;
        }
        transform.rotation = Quaternion.Euler(_target);
    }

    IEnumerator Jump()
    {
        rb.angularVelocity = Vector3.zero;
        rb.velocity *= velocityDampingOnJump;
        Vector3 movement = Vector3.up * jumpHeight;
        rb.AddForce(movement, ForceMode.Impulse);
        yield return new WaitForSeconds(0.1f);
        state = FighterStates.Jumping;
        StartCoroutine(Roll(360f));
    }

    IEnumerator Roll(float _deg)
    {
        float degToRotate = (360f / jumpRollSpeed) * Time.deltaTime;
        transform.RotateAround(transform.position, transform.right, degToRotate);
        _deg -= degToRotate;

        yield return new WaitForFixedUpdate();

        if (_deg >= 0)
            StartCoroutine(Roll(_deg));
    }

    IEnumerator HitTimeout()
    {
        state = FighterStates.Confused;
        yield return new WaitForSeconds(confusedAfterHitTime);
        state = FighterStates.Moving;
    }

    IEnumerator ActionTimeout(float _time)
    {
        canTakeAction = false;
        yield return new WaitForSeconds(_time);
        canTakeAction = true;
    }

    IEnumerator JumpAttackCooldown(float _time)
    {
        canJumpAttack = false;
        yield return new WaitForSeconds(_time);
        canJumpAttack = true;
    }

    IEnumerator JumpCooldown(float _time) {
        canJump = false;
        yield return new WaitForSeconds(_time);
        canJump = true;
    }

    IEnumerator DashCooldown(float _time)
    {
        canDash = false;
        yield return new WaitForSeconds(_time);
        canDash = true;
    }

    IEnumerator LoseLife()
    {
        state = FighterStates.Dead;
        lifesLeft--;
        changedLifepoints?.Invoke(lifesLeft);

        if (!IsAlife())
        {
            Lose();
            yield break;
        }

        yield return new WaitForSeconds(respawnTime / 2);
        ResetPosition();
        float waitedTime = 0f;
        Vector3 holdingPos = transform.position;

        while (waitedTime < respawnTime / 2)
        {
            waitedTime += Time.deltaTime;
            transform.position = holdingPos;
            yield return null;
        }

        state = FighterStates.Jumping;
    }

    IEnumerator LastHitReset()
    {
        yield return new WaitForSeconds(lasthitResetTime);
        lastHit = null;
    }

    private void OnCollisionEnter(Collision other)
    {
        jumpIntersected = true;

        if (other.collider.CompareTag("Bounds") && state != FighterStates.Dead)
        {
            if (lastHit)
            {
                lastHit.KilledEnemy();
                lastHit = null;
            }

            StartCoroutine(LoseLife());
        }

        if (other.collider.CompareTag("Player"))
        {
            if (state == FighterStates.Dashing || state == FighterStates.JumpAttack)
            {
                lastHit = other.gameObject.GetComponent<CubeController>();
                Vector3 dir = (other.transform.position - transform.position).normalized;
                other.gameObject.GetComponent<Rigidbody>().AddForce(dir * jumpAttackForce, ForceMode.Impulse);

                StartCoroutine(LastHitReset());

                other.gameObject.SendMessage("GetHit", gameObject);
                state = FighterStates.Moving;
            }
        }

        if (other.collider.CompareTag("Ground"))
        {
            isOnGround = true;
            if (state == FighterStates.JumpAttack)
            {
                Collider[] hitColliders = Physics.OverlapSphere(transform.position, jumpAttackRadius);
                foreach (var hitCollider in hitColliders)
                {
                    if (hitCollider.CompareTag("Player") && hitCollider.gameObject != gameObject)
                    {
                        Vector3 dir = (hitCollider.transform.position - transform.position).normalized;
                        hitCollider.GetComponent<Rigidbody>().AddForce(dir * jumpAttackForce, ForceMode.Impulse);
                        hitCollider.gameObject.SendMessage("GetHit", gameObject);
                    }
                }
                StartCoroutine(JumpCooldown(jumpCooldown));
            }

            if(state == FighterStates.Jumping) {
                StartCoroutine(JumpCooldown(jumpCooldown));
            }
        }
        else
            dashIntersected = true;
    }

    private void OnCollisionStay(Collision other)
    {
        if (other.collider.CompareTag("Ground"))
            isOnGround = true;
    }

    private void OnCollisionExit(Collision other)
    {
        if (other.collider.CompareTag("Ground"))
        {
            isOnGround = false;
        }
    }
}
