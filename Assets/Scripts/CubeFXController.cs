﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

[RequireComponent(typeof(CubeController))]
public class CubeFXController : MonoBehaviour
{
    CubeController controller;

    public VisualEffect confused = null;
    public VisualEffect dash = null;
    public float dashTime = 2f;

    void Start()
    {
        controller = GetComponent<CubeController>();
        // controller.Dashing += Dash;
    }

    void Update()
    {
        if (controller.state == FighterStates.Confused)
        {
            confused.SendEvent("OnPlay");
        }
        else
        {
            confused.SendEvent("OnStop");
        }

        if (controller.state == FighterStates.Dashing)
        {
            dash.SendEvent("OnPlay");
        }
        else
        {
            dash.SendEvent("OnStop");
        }
    }

    void Dash()
    {
        StartCoroutine(Dashing());
    }

    IEnumerator Dashing()
    {
        dash.SendEvent("Play");
        yield return new WaitForSeconds(dashTime);
        dash.SendEvent("OnStop");
    }
}
