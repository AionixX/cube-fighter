﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class GameController : MonoBehaviour
{
    public bool isTraining = false;
    public List<CubeController> players = new List<CubeController>();
    public CinemachineTargetGroup targetGroup = null;
    public MenuController menuController = null;
    public void PlayerLost(CubeFighter fighter)
    {
        if (!isTraining)
            targetGroup.RemoveMember(fighter.transform);

        fighter.AddReward(-1.0f);

        if (PlayersAlive() > 1)
        {
            if (!isTraining)
            {
                if (fighter.isPlayer)
                {
                    menuController.OpenGameOverMenu(false);
                }
            }
        }
        else
        {
            CubeController lastCube = GetPlayerAlife();
            lastCube.AddReward(1.0f);

            if (isTraining)
            {
                ResetGame();
            }
            else
            {
                menuController.OpenGameOverMenu(lastCube.isPlayer);
            }
        }


    }

    private void RemoveAllMemberFromTargetGroup()
    {
        foreach (CubeController cube in players)
        {
            targetGroup.RemoveMember(cube.transform);
        }
    }

    private void AddAllMemberFromTargetGroup()
    {
        foreach (CubeController cube in players)
        {
            targetGroup.AddMember(cube.transform, 1f, 10f);
        }
    }


    private int PlayersAlive()
    {
        int playerLeftCount = 0;

        foreach (CubeController cube in players)
        {
            if (cube.IsAlife())
                playerLeftCount++;
        }

        return playerLeftCount;
    }

    private CubeController GetPlayerAlife()
    {
        foreach (CubeController cube in players)
        {
            if (cube.IsAlife())
                return cube;
        }
        return null;
    }

    public void ResetGame()
    {
        if (!isTraining)
        {
            RemoveAllMemberFromTargetGroup();
            AddAllMemberFromTargetGroup();
        }

        foreach (CubeController cube in players)
        {
            cube.EndEpisode();
        }
    }
}
