﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputController : MonoBehaviour
{
    #region SINGLETON PATTERN
    private static InputController _instance;
    public static InputController Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<InputController>();

                if (_instance == null)
                {
                    GameObject container = new GameObject("Bicycle");
                    _instance = container.AddComponent<InputController>();
                }
            }

            return _instance;
        }
    }
    #endregion

    [SerializeField]
    private FixedJoystick joystick = null;

    [SerializeField]
    private ButtonController dashButton = null;

    [SerializeField]
    private ButtonController jumpButton = null;

    public float GetVertical()
    {
        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.S))
            return Input.GetAxis("Vertical");
        

        if (joystick != null) {
            Debug.Log("Joystick");
            return joystick.Direction.y;
        }
        else {
            Debug.Log("Return null");
            return 0f;
        }
    }

    public float GetHorizontal()
    {
        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D))
            return Input.GetAxis("Horizontal");

        if (joystick)
            return joystick.Direction.x;
        else
            return 0f;
    }

    public bool GetDash()
    {
        if (Input.GetKey(KeyCode.F))
            return true;

        if (dashButton)
            return dashButton.buttonPressed;
        else
            return false;
    }

    public bool GetJump()
    {
        if (Input.GetKey(KeyCode.Space))
            return true;

        if (jumpButton)
            return jumpButton.buttonPressed;
        else
            return false;

    }

}
