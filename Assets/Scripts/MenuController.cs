﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuController : MonoBehaviour
{
    public GameObject startMenu = null;
    public GameObject pauseMenu = null;
    public GameObject gameOverMenu = null;
    public GameObject controllUI = null;
    public GameController gameController = null;
    
    public void OpenPauseMenu() {
        Time.timeScale = 0;
        pauseMenu.SetActive(true);
        controllUI.SetActive(false);
        gameOverMenu.SetActive(false);
        startMenu.SetActive(false);
    }

    public void OpenGameOverMenu(bool playerWon) {
        Time.timeScale = 0;
        pauseMenu.SetActive(false);
        controllUI.SetActive(false);
        gameOverMenu.SetActive(true);
        startMenu.SetActive(false);
        Debug.Log("Player won: " + playerWon);
    }

    public void OpenStartMenu() {
        Time.timeScale = 0;
        pauseMenu.SetActive(false);
        controllUI.SetActive(false);
        gameOverMenu.SetActive(false);
        startMenu.SetActive(true);
        gameController.ResetGame();
    }

    public void ClosePauseMenu() {
        Time.timeScale = 1;
        pauseMenu.SetActive(false);
        controllUI.SetActive(true);
        gameOverMenu.SetActive(false);
        startMenu.SetActive(false);
    }

    public void CloseGameOverMenu() {
        Time.timeScale = 0;
        pauseMenu.SetActive(false);
        controllUI.SetActive(false);
        gameOverMenu.SetActive(false);
        startMenu.SetActive(true);
    }

    public void CloseStartMenu() {
        Time.timeScale = 1;
        pauseMenu.SetActive(false);
        controllUI.SetActive(true);
        gameOverMenu.SetActive(false);
        startMenu.SetActive(false);
    }
}
